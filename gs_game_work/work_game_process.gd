extends Node


func _ready():
	
	Logger.trace("[WorkGameProcess] _ready")
	
	Game.on_game_process.connect(on_process)
	Game.on_game_quitting.connect(on_quit)
	Game.start_game()
	
	Game.show_game_settings()	
	Game.show_user_settings()
	
	$VBoxContainer/NoneButton.button_down.connect(on_none_button)
	$VBoxContainer/StartButton.button_down.connect(on_start_button)
	$VBoxContainer/StopButton.button_down.connect(on_stop_button)
	$VBoxContainer/IdleButton.button_down.connect(on_idle_button)


func on_process(state: int , delta: float):
	
	Logger.fine("[WorkGameProcess] on_process")
	
	$VBoxContainer/StateLabel.text = "STATE : %d" % Game.__game_state
	$VBoxContainer/LastState.text = "LAST : %d" % Game.__last_game_state
	$VBoxContainer/NextState.text = "NEXT : %d" % Game.__next_game_state
	$VBoxContainer/TickLabel.text = "TICKS : %d" % Game.__game_ticks
	
#	Logger.info(str(state))
	
	match state:
		
		Game.GameStates.NONE:
			Logger.fine("- none state")
			
		Game.GameStates.IDLE:
			Logger.fine("- idle state")
			
		Game.GameStates.START:
			Logger.fine("- start state")
			
		Game.GameStates.STOP:
			Logger.fine("- stop state")
			
		
func on_quit():
	Logger.trace("[WorkGameProcess] on_quit")

func on_idle():
	Logger.trace("[WorkGameProcess] on_idle")
	
func on_start():
	Logger.trace("[WorkGameProcess] on_start")

func on_stop():
	Logger.trace("[WorkGameProcess] on_stop")
	
func on_none_button():
	Game.set_state(Game.GameStates.NONE)

func on_start_button():
	Game.set_state(Game.GameStates.START)
	
func on_stop_button():
	Game.set_state(Game.GameStates.STOP)
		
func on_idle_button():
	Game.set_state(Game.GameStates.IDLE)
	
