extends Node

func _ready():
	
	var _count = 100000
	
	var _things = []
	
	var _start: float = Time.get_ticks_msec()
	for i in range(_count):
		var _thing = Thing.new()
		_things.append(_thing)
		
	var _time: float = Time.get_ticks_msec() - _start
	
	print("It took %3.3f seconds to create %d Things." % [_time/1000, _count])
	
	print()
	print("Top 10 Things")
	print("=============")
	
	for i in range(10):
		print(_things[i].id)
	
	print()
	
	Game.quit()
