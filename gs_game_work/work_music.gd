extends Node

func _ready():
	
	Game.load_music($Music)
	$Container/VBoxContainer/Music1.button_down.connect(_on_music1)
	$Container/VBoxContainer/Music2.button_down.connect(_on_music2)
	$Container/VBoxContainer/Stop.button_down.connect(_on_stop)

func _on_music1():
	Game.play_music("Music1")
	

func _on_music2():
	Game.play_music("Music2")
	

func _on_stop():
	Game.stop_music()
