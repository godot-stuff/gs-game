extends Node

func _ready():
	
	Game.load_sounds($Sounds)
	$Container/VBoxContainer/Sound1.button_down.connect(_on_sound1)
	$Container/VBoxContainer/Sound2.button_down.connect(_on_sound2)
	$Container/VBoxContainer/Sound3.button_down.connect(_on_sound3)

func _on_sound1():
	Game.play_sound("Sound1")
	

func _on_sound2():
	Game.play_sound("Sound2")


func _on_sound3():
	Game.play_sound("Sound3")
