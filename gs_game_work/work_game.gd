extends Node


var __game_settings_filename = "res://gs_game_work/game.ini"


func _ready():
	
	Logger.trace("[WorkGame] _ready")
	
	Game.load_game_settings(__game_settings_filename)
	Game.show_game_settings()	
	Game.show_user_settings()
	Game.start_game()
	
	Logger.trace(Game.get_game_setting("name"))
	Logger.trace(str(Game.get_user_setting("master_volume")))
	
	Game.user_settings["player1"] = "sp0CkEr2"
	Game.user_settings["player2"] = "Ar50n1k"
	Game.user_settings["player3"] = "R0wdii"
	
	Game.quit()
