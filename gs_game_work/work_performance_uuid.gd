extends Node

func _ready():
	
	var _count = 100000
	
	var _start: float = Time.get_ticks_msec()
	for i in range(_count):
		var _uuid = UUID.new()
		
	var _time: float = Time.get_ticks_msec() - _start
	
	print("It took %3.3f seconds to create %d UUID objects" % [_time/1000, _count])
	
	Game.quit()
