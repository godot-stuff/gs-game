extends Node

signal on_game_settings_loaded()
signal on_user_settings_loaded()
signal on_game_loaded()
signal on_game_quitting()
signal on_game_process(game_state: GameStates, delta: float)
signal on_game_physics_process(game_state: GameStates, delta: float)

enum GameStates 
{
	NONE,
	IDLE,
	INIT,
	HOME,
	ABOUT,
	HELP,
	SCORES,
	OPTIONS,
	PLAY,
	WIN,
	LOSE,
	START,
	STOP,
	PAUSE,
	QUIT
}


var __quit_cancel: bool = false
var __is_quitting: bool = false
var __game_settings_data = {}
var __game_settings_filename: String = "res://game.ini"
var __user_settings_data = {}
var __user_settings_filename: String = "user://user.ini"
var __game_state: GameStates = GameStates.NONE
var __next_game_state: GameStates = GameStates.NONE
var __last_game_state: GameStates = GameStates.NONE
var __use_physics: bool = false
var __game_ticks: int = 0
var __music: Node
var __sounds: Node
var __current_music: AudioStreamPlayer


const VERSION="4.3-DEV"


var tick:
	get: return __game_ticks
	
var game_settings: Dictionary:
	get: return __game_settings_data
	set(value): __game_settings_data = value
	
var user_settings: Dictionary:
	get: return __user_settings_data
	set(value): __user_settings_data = value
	

func _exit_tree():

	print()
	print("*** game ended ***")
	print()
	

func _init():
	
	Logger.fine("[Game] _init")
	
	print(" ")
	print("godot-stuff Game")
	print("https://gitlab.com/godot-stuff/gs-game")
	print("Copyright 2018-2023, SpockerDotNet LLC")
	print("Version " + VERSION)
	print(" ")
	
	randomize()


func _notification(what: int):
	
	if __is_quitting:
		return
	
	Logger.fine("[Game] _notification")
	
	match what:
		
		Node.NOTIFICATION_WM_CLOSE_REQUEST:
			Logger.trace("- received NOTIFICATION_WM_CLOSE_REQUEST")
			quit()
			

func _process(delta: float):
	
	Logger.fine("[Game] _process")
	
	__process__(delta)
	on_game_process.emit(__game_state, delta)
	

func _physics_process(delta: float):
	
	Logger.fine("[Game] _physics_process")
	
	if __use_physics:
		on_game_physics_process.emit(__game_state, delta)
		
		
func _ready():
	
	Logger.trace("[Game] _ready")
	
	load_game_settings(__game_settings_filename)
	load_user_settings(__user_settings_filename)
	
	set_physics_process(false)
	set_process(false)


##
## Cancel a previous quit game request.
##
func cancel_quit():
	__quit_cancel = true

	
func get_game_setting(key: String):
	
	Logger.trace("[Game] get_game_setting")

	if __game_settings_data.has(key):
		return __game_settings_data[key]
		
	Logger.warn("- game setting %s not found" % key)
	return null
	
	
func get_user_setting(key: String):
	
	Logger.trace("[Game] get_user_setting")
	
	if __user_settings_data.has(key):
		return __user_settings_data[key]
		
	Logger.warn("- user setting %s not found" % key)
	return null
	
	
func load_game_settings(config_filename: String):
	
	Logger.trace('[Game] load_game_settings')
	
	Logger.trace('- loading game settings from configuration file %s' % config_filename)
	
	__game_settings_filename = config_filename

	if __game_settings_data.size() == 0:	
		
		# default settings
		__game_settings_data["name"] = "My Game"
		__game_settings_data["version"] = "1.0.0"
		__game_settings_data["long_version"] = "Version 1.0"
		__game_settings_data["copyright"] = "Copyright 2023 My Company. All rights reserved."
		__game_settings_data["company"] = "My Company"
		__game_settings_data["publisher"] = "My Publisher"
	
	# load the settings if the file was valid
	var _settings = Configuration.load_config(config_filename) as Dictionary

	for _key in _settings.keys():
		__game_settings_data[_key] = _settings[_key]
		
	on_game_settings_loaded.emit()
	

func load_user_settings(config_filename: String) -> void:
	
	Logger.trace('[Game] load_user_settings')
	
	Logger.trace('- loading user settings from configuration file %s' % config_filename)
	
	__user_settings_filename = config_filename
	
	if __user_settings_data.size() == 0:
		
		# default settings
		__user_settings_data["music_volume"] = 1.0
		__user_settings_data["sound_volume"] = 1.0
		__user_settings_data["master_volume"] = 1.0
		__user_settings_data["hi_score"] = 0
		
	# load the settings if the file was valid
	var _settings = Configuration.load_config(config_filename) as Dictionary

	for _key in _settings.keys():
		__user_settings_data[_key] = _settings[_key]

	on_user_settings_loaded.emit()

func load_music(node: Node) -> void:

	__music = node	
	
func play_music(name : String) -> void:

	stop_music()
		
	__current_music = __music.get_node(name) as AudioStreamPlayer
	__current_music.play()	
	

func stop_music() -> void:
	
	if (__current_music != null):
		__current_music.stop()
			
	
func load_sounds(node: Node) -> void:
	
	__sounds = node
	
	
func play_sound(name: String, volume = -10) -> void:
	
	var _sound = __sounds.get_node(name) as AudioStreamPlayer
	
	if _sound:
		
		var _player = AudioStreamPlayer.new() as AudioStreamPlayer
		add_child(_player)
		
		_player.volume_db = volume
		_player.stream = _sound.stream
		_player.name = "%s_1" % _sound.name
		_player.play() 
		
		await _player.finished
		_player.queue_free()
				
	else:
		
		Logger.warn("- sound %s not found" % name)

##
## Quit your game gracefully.
##
func quit():
	
	var _state = __game_state
	
	Logger.trace("[Game] quit")
	
	Logger.trace("- trying to quit the game")
	on_game_quitting.emit()
	
	if __quit_cancel:
		Logger.trace("- attempt to quit the game was cancelled")
		__quit_cancel = false
		set_state(__last_game_state)
		return
		
	__is_quitting = true
	set_state(GameStates.QUIT)


func set_state(state):
	Logger.fine("[Game] set_state")
	__next_game_state = state	
	
	
func show_game_settings():
	
	Logger.trace("[Game] show_game_settings")
	
	print("")
	print("Game Settings")
	print("=============")

	__show_settings__(__game_settings_data)
	
	print("")		


func show_user_settings():
	
	Logger.trace("[Game] show_user_settings")
	
	print("")
	print("User Settings")
	print("=============")

	__show_settings__(__user_settings_data)
	
	print("")		


func start_game():
	
	Logger.trace("[Game] start_game")
		
	set_process(true)
	get_tree().auto_accept_quit = false
	
	set_state(GameStates.INIT)
	
	
##
## Kill the game.
##
func __kill__():
	
	get_tree().quit(0)
	

func __process__(delta):
	
	Logger.fine("[Game] __process__")
	
	match __game_state:
		
		GameStates.QUIT:
			if __game_ticks > 0:
				__quit__()

	__game_ticks += 1
	
	if __next_game_state != __game_state:
		
		__last_game_state = __game_state			
		__game_state = __next_game_state


func __quit__():
	
	# save user settings
	__save_settings__(__user_settings_data)
	
	# notify other nodes
	get_tree().get_root().propagate_notification(NOTIFICATION_WM_CLOSE_REQUEST)
	
	# kill the game
	__kill__()

	
func __save_settings__(settings: Dictionary) -> void:
	
	Logger.trace("[Game] __save_settings__")
	Configuration.save_config(__user_settings_filename, settings)

	
func __show_settings__(settings: Dictionary) -> void:
	
	var _keys = settings.keys()
	
	print("")
	print("Found %s Settings" % _keys.size())
	print("")
	
	for _key in _keys: 
		print("%s = %s" % [_key, settings[_key]] )
