class_name Configuration
extends Node


static func load_config(config_file : String) -> Dictionary:
	
	Logger.trace("[Configuration] load_config")
	var _config = ConfigFile.new()
	var _config_filename = config_file
	var _settings: Dictionary = {}

	if not FileAccess.file_exists(_config_filename):
		Logger.warn("Configuration File {name} is missing.".format({"name": _config_filename}))
		return _settings

	Logger.debug(_config_filename)
	var _err = _config.load(_config_filename)
	
	if (_err > 0):
		Logger.error("Error {0} Reading Configuration File.".format([_err]))
		return _settings
		
	for _key in _config.get_section_keys("settings"):
		_settings[_key] = _config.get_value("settings", _key)

	return _settings


static func save_config(config_file: String, config_data: Dictionary) -> void:
	
	Logger.trace("[Configuration] save_config")
	
	var _config = ConfigFile.new()
	
	for _key in config_data.keys():
		_config.set_value("settings", _key, config_data[_key])

	_config.save(config_file)
