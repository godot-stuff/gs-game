## A Thing is Something that exists in a Game
##
## A Thing is Nothing but it is Everything. For that
## reason, Everything in the Game must be a Thing.
##
class_name Thing
extends RefCounted


var __id__: UUID
var __active__: bool
var __visible__: bool

func _init():
	__id__ = UUID.new()	


var id:
	get: return __id__.uuid


var active:
	get: return __active__
	set(value): __active__ = value
			

var visible:
	get: return __visible__
	set(value): __visible__ = value
			
