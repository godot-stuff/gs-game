@tool
extends EditorPlugin


func _enter_tree():
	add_autoload_singleton("Game", "res://addons/gs_game/game.gd")
	
	
func _exit_tree():
	remove_autoload_singleton("Game")
